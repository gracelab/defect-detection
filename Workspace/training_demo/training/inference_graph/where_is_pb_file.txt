The frozen_inference_graph.pb file was too large to include in this
repository, but by using frozen_inference_graph.pbtxt in
Workspace\training_demo\training, you can convert it into the .pb
form using the Python script at the link below:
https://gist.github.com/alsrgv/1a83c7953cb04676f6a0b3ec3917a18c