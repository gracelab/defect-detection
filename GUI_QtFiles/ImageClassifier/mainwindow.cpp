#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QFileDialog"
#include "QGraphicsView"
#include "QDirIterator"
#include <opencv2/opencv.hpp>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->ornlLogo->setPixmap(QPixmap("C:/Users/4gl/Documents/QtThings/ImageClassifier/ORNL_Logos/ornl_mdf_color_sm.png"));
    ui->imageView->setScene(&scene);
    scene.addItem(&pixmap);
    ui->tabs->setCurrentIndex(0);
    ui->progressBar->setValue(0);
    zoomdial = ui->zoomDial->value();
    zoomslider = ui->zoomSlider->value();

}

void MainWindow::on_browseImageBtn_clicked()
{

    if(!(ui->imageRadio->isChecked()) & ui->imgFolRadio->isChecked()){
        ui->imageRadio->toggle();
        ui->imgFolRadio->toggle();
    }

    if(ui->imageRadio->isChecked() & ui->imgFolRadio->isChecked()){

        ui->imgFolRadio->toggle();
    }

    if(!(ui->imageRadio->isChecked()) & !(ui->imgFolRadio->isChecked())){

        ui->imageRadio->toggle();
    }

    QString imagePath = QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
                "",
                tr("PNG (*.png);;JPEG (*.jpg *.jpeg);;TIF (*.tif)"));


    imageObject = new QImage();
    imageObject->load(imagePath);
    ui->videoEdit->setText(imagePath);
    ui->filePathName->setText(imagePath);
    singleImgHold = imagePath;
    ui->lineEdit->setText("");
    image = QPixmap::fromImage(*imageObject);

    //scene = new QGraphicsScene(this);
    //scene->addPixmap(image);
    //scene->setSceneRect(image.rect());
    //ui->imageView->setScene(scene);

    if(!scene.items().isEmpty()){
        int len = scene.items().length();
        for(int i = 0; i < len; i++){
            scene.removeItem(scene.items()[0]);
        }
    }
    scene.addPixmap(image);
    scene.setSceneRect(image.rect());
    ui->numDefectEdit->setText("");
}

void MainWindow::on_browseImgFolBtn_clicked()
{


    if(!(ui->imgFolRadio->isChecked()) & ui->imageRadio->isChecked()){
        ui->imageRadio->toggle();
        ui->imgFolRadio->toggle();
    }

    if(ui->imageRadio->isChecked() & ui->imgFolRadio->isChecked()){

        ui->imageRadio->toggle();
    }

    if(!(ui->imageRadio->isChecked()) & !(ui->imgFolRadio->isChecked())){

        ui->imgFolRadio->toggle();
    }

    if(!ourImages.isEmpty()){
        int length = ourImages.length();
        for(int i = 0; i < length; i++){

        ourImages.removeLast();

        }

    }

    if(!pixList.isEmpty()){
        int length = pixList.length();
        for(int i = 0; i < length; i++){

        pixList.removeLast();

        }

    }


    QString folderPath = QFileDialog::getExistingDirectory(this,
                                                           tr("Open Directory"),
                                                           "",
                                                           QFileDialog::ShowDirsOnly);

    if(folderPath.length() != 0){
    ui->lineEdit->setText(folderPath);
    ui->videoEdit->setText("");

    QDirIterator it(folderPath, QStringList(),
                    QDir::Files, QDirIterator::Subdirectories);


    while(it.hasNext())
    {
        qDebug() << it.next();
        ourImages.append(it.filePath());

    }


        place = 0;
        QString imagePath = ourImages[0];

        imageObject = new QImage();
        imageObject->load(imagePath);

        ui->filePathName->setText(imagePath);
        image = QPixmap::fromImage(*imageObject);

//        scene = new QGraphicsScene(this);
//        scene->addPixmap(image);
//        scene->setSceneRect(image.rect());
//        ui->imageView->setScene(scene);
        if(!scene.items().isEmpty()){
            int len = scene.items().length();
            for(int i = 0; i < len; i++){
                scene.removeItem(scene.items()[0]);
            }
        }
        scene.addPixmap(image);
        scene.setSceneRect(image.rect());

        ui->numDefectEdit->setText("");
    }

}

void MainWindow::on_prevButton_clicked()
{
    if(!(scene.items().isEmpty())){
    if(!(ui->imageRadio->isChecked()))
    {
        if(!pixList.isEmpty()){
            if(pixPlace-1 >= 0){
                pixPlace = pixPlace-1;
                scene.clear();
                QString file = ourImages[pixPlace];
                int idx = file.lastIndexOf("/");

                QString suff = file.mid(idx+1);
                int idxx = suff.lastIndexOf(".");

                QString exten = suff.mid(idxx);
                suff = suff.mid(0, idxx);

                QString newname = suff + "_boxes" + exten;

                ui->filePathName->setText(newname);
                if(ui->valueChoice->currentIndex() == 0){


                ui->numDefectEdit->setText(QString::number(numDefectList[pixPlace]));
                }
                scene.addPixmap(pixList[pixPlace]);
            }
        }
        else{
    if(place-1 >= 0)
    {
     place = place-1;
     QString imagePath = ourImages[place];
     imageObject = new QImage();
     imageObject->load(imagePath);
     ui->filePathName->setText(imagePath);
     image = QPixmap::fromImage(*imageObject);

//     scene = new QGraphicsScene(this);
//     scene->addPixmap(image);
//     scene->setSceneRect(image.rect());
//     ui->imageView->setScene(scene);
     if(!scene.items().isEmpty()){
         int len = scene.items().length();
         for(int i = 0; i < len; i++){
             scene.removeItem(scene.items()[0]);
         }
     }
     scene.addPixmap(image);
     scene.setSceneRect(image.rect());
     }
   }
  }
}
}

void MainWindow::on_nextButton_clicked()
{
    if(!(scene.items().isEmpty())){
    if(!(ui->imageRadio->isChecked()))
    {
        if(!pixList.isEmpty()){
            if(pixPlace+1 < pixList.length()){
                pixPlace = pixPlace+1;
                scene.clear();
                QString file = ourImages[pixPlace];
                int idx = file.lastIndexOf("/");

                QString suff = file.mid(idx+1);
                int idxx = suff.lastIndexOf(".");

                QString exten = suff.mid(idxx);
                suff = suff.mid(0, idxx);

                QString newname = suff + "_boxes" + exten;

                ui->filePathName->setText(newname);
                if(ui->valueChoice->currentIndex() == 0){
                ui->numDefectEdit->setText(QString::number(numDefectList[pixPlace]));
                }
                scene.addPixmap(pixList[pixPlace]);
            }
        }
        else{
    if(place+1 < ourImages.length())
    {
     place = place+1;
     QString imagePath = ourImages[place];
     imageObject = new QImage();
     imageObject->load(imagePath);
     ui->filePathName->setText(imagePath);
     image = QPixmap::fromImage(*imageObject);

//     scene = new QGraphicsScene(this);
//     scene->addPixmap(image);
//     scene->setSceneRect(image.rect());
//     ui->imageView->setScene(scene);
     if(!scene.items().isEmpty()){
         int len = scene.items().length();
         for(int i = 0; i < len; i++){
             scene.removeItem(scene.items()[0]);
         }
     }
     scene.addPixmap(image);
     scene.setSceneRect(image.rect());
   }
    }
    }
}
}

void MainWindow::on_startBtn_clicked()
{


     using namespace cv;
     using namespace dnn;
     using namespace std;

    const int inWidth = 500;
    const int inHeight = 500;
    const float meanVal = 127.5;
    const float inScaleFactor = 1.0f / meanVal;
    const float confidenceThreshold = 0.5f;


    QMap<int, QString> classNames;
    QFile labelsFile(ui->classesFileEdit->text());
    if(labelsFile.open(QFile::ReadOnly | QFile::Text)){

        while(!labelsFile.atEnd()){

            QString line = labelsFile.readLine();
            classNames[line.split(',')[0].trimmed().toInt()] = line.split(',')[1].trimmed();

        }

        labelsFile.close();

    }

    QElapsedTimer timer;
    timer.start();
    ui->progressLabel->setText("Loading network...");
    tfNetwork = cv::dnn::readNetFromTensorflow(ui->pbFileEdit->text().toStdString(),
                                      ui->pbtxtFileEdit->text().toStdString());
    qint64 elapsed = timer.elapsed();
    ui->progressLabel->setText("Network loaded");

    QMessageBox::information(this, "Info", QString("Network is loaded in %1 ms").arg(elapsed));
    ui->progressLabel->setText("Processing images...");

    if(ui->imageRadio->isChecked()){

        int numDefects = 0;
        ui->progressBar->setMinimum(0);
        ui->progressBar->setMaximum(100);

        //QString qs = ui->filePathName->text();
        QString qs = singleImgHold;

        const cv::String pathName = qs.toLocal8Bit().constData();

        Mat image = imread(pathName, 1);

        Mat inputBlob = blobFromImage(image,
                                      1.0,
                                      Size(inWidth, inHeight),
                                      Scalar(0.0, 0.0, 0.0),
                                      true,
                                      false);

        tfNetwork.setInput(inputBlob);
        cout << "set blob";
        Mat result = tfNetwork.forward();
        cout << "went forwardS";
        Mat detections(result.size[2], result.size[3], CV_32F, result.ptr<float>());






        for(int i = 0; i<detections.rows; i++){



            float confidence = detections.at<float>(i, 2);



           if(confidence > confidenceThreshold){

               numDefects++;

                using namespace cv;

                int objectClass = (int)(detections.at<float>(i,1));

                int left = static_cast<int>(detections.at<float>(i, 3) * image.cols);
                int right = static_cast<int>(detections.at<float>(i, 5) * image.cols);
                int top = static_cast<int>(detections.at<float>(i, 4) * image.rows);
                int bottom = static_cast<int>(detections.at<float>(i, 6) * image.rows);

                rectangle(image, Point(left, top), Point(right, bottom), Scalar(0, 255, 0), 2);
//                String label = classNames[objectClass].toStdString();
//                int baseLine = 0;
//                Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
//                top = max(top, labelSize.height);
//                rectangle(image, Point(left, top - labelSize.height),
//                          Point(left + labelSize.width, top + baseLine),
//                          Scalar(255, 255, 255), FILLED);
//                putText(image, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0,0,0), 1);


            }
        }


            ui->numDefectEdit->setText(QString::number(numDefects));
            scene.addPixmap(QPixmap::fromImage(QImage(image.data,
                                                      image.cols,
                                                      image.rows,
                                                      image.step,
                                                      QImage::Format_RGB888).rgbSwapped()));
            //QString file = ui->filePathName->text();
            QString file = singleImgHold;
            int idx = file.lastIndexOf("/");

            QString suff = file.mid(idx+1);
            int idxx = suff.lastIndexOf(".");

            QString exten = suff.mid(idxx);
            suff = suff.mid(0, idxx);

            QString newname = suff + "_boxes" + exten;

            ui->filePathName->setText(newname);

            single = QImage(image.data,
                            image.cols,
                            image.rows,
                            image.step,
                            QImage::Format_RGB888).rgbSwapped();
            ui->progressLabel->setText("Images Processed");
            ui->progressBar->setValue(100);
            qApp->processEvents();

    }

    if(ui->imgFolRadio->isChecked()){
        ui->progressBar->setMinimum(0);
        ui->progressBar->setMaximum(ourImages.length());
        for(int i = 0; i < ourImages.length(); i++){

            int numDefects2 = 0;

            QString qs = ourImages[i];

            const cv::String pathName = qs.toLocal8Bit().constData();

            Mat image = imread(pathName, 1);

            Mat inputBlob = blobFromImage(image,
                                          1.0,
                                          Size(inWidth, inHeight),
                                          Scalar(0.0, 0.0, 0.0),
                                          true,
                                          false);

            tfNetwork.setInput(inputBlob);
            Mat result = tfNetwork.forward();
            Mat detections(result.size[2], result.size[3], CV_32F, result.ptr<float>());

            for(int i = 0; i<detections.rows; i++){

                float confidence = detections.at<float>(i, 2);

               if(confidence > confidenceThreshold){

                    numDefects2++;

                    using namespace cv;

                    int objectClass = (int)(detections.at<float>(i,1));

                    int left = static_cast<int>(detections.at<float>(i, 3) * image.cols);
                    int right = static_cast<int>(detections.at<float>(i, 5) * image.cols);
                    int top = static_cast<int>(detections.at<float>(i, 4) * image.rows);
                    int bottom = static_cast<int>(detections.at<float>(i, 6) * image.rows);

                    rectangle(image, Point(left, top), Point(right, bottom), Scalar(0, 255, 0), 2);
    //                String label = classNames[objectClass].toStdString();
    //                int baseLine = 0;
    //                Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
    //                top = max(top, labelSize.height);
    //                rectangle(image, Point(left, top - labelSize.height),
    //                          Point(left + labelSize.width, top + baseLine),
    //                          Scalar(255, 255, 255), FILLED);
    //                putText(image, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0,0,0), 1);

                }
            }

            numDefectList.append(numDefects2);

            procImages.append(QImage(image.data,
                                     image.cols,
                                     image.rows,
                                     image.step,
                                     QImage::Format_RGB888).rgbSwapped());
            pixList.append(QPixmap::fromImage(QImage(image.data,
                                                     image.cols,
                                                     image.rows,
                                                     image.step,
                                                     QImage::Format_RGB888).rgbSwapped()));

            ui->progressBar->setValue(pixList.length());


        }

        ui->progressLabel->setText("Images Processed");
        pixPlace = 0;

        QString file = ourImages[pixPlace];
        int idx = file.lastIndexOf("/");

        QString suff = file.mid(idx+1);
        int idxx = suff.lastIndexOf(".");

        QString exten = suff.mid(idxx);
        suff = suff.mid(0, idxx);

        QString newname = suff + "_boxes" + exten;

        ui->filePathName->setText(newname);

        ui->valueChoice->setCurrentIndex(0);
        ui->numDefectEdit->setText(QString::number(numDefectList[pixPlace]));

        scene.addPixmap(pixList[pixPlace]);
        qApp->processEvents();
    }

}

void MainWindow::on_pbBrowseBtn_clicked(){
    QString filePath = QFileDialog::getOpenFileName(this, "Open Model", QString(), "PB files (*.pb)");
    if(QFile::exists(filePath)){
        ui->pbFileEdit->setText(filePath);
    }
}

void MainWindow::on_pbtxtBrowseBtn_clicked(){
    QString filePath = QFileDialog::getOpenFileName(this, "Open Config", QString(), "PBTXT files (*.pbtxt)");
    if(QFile::exists(filePath)){
        ui->pbtxtFileEdit->setText(filePath);
    }
}

void MainWindow::on_classesBrowseBtn_clicked(){
    QString filePath = QFileDialog::getOpenFileName(this, "Open Classes", QString(), "All files (*.*)");
    if(QFile::exists(filePath)){
        ui->classesFileEdit->setText(filePath);
    }
}

void MainWindow::on_browseSaveBtn_clicked(){
    QString folderPath = QFileDialog::getExistingDirectory(this,
                                                         tr("Open Directory"),
                                                         "",
                                                         QFileDialog::ShowDirsOnly);
    ui->saveEdit->setText(folderPath);
}

void MainWindow::on_saveButton_clicked(){

    if((!(ui->saveEdit->text().isEmpty())) ){

        QString folderPath = ui->saveEdit->text();

    if(ui->imageRadio->isChecked() && (!(single.isNull()))){

        QString file = ui->videoEdit->text();
        int idx = file.lastIndexOf("/");

        QString suff = file.mid(idx+1);
        int idxx = suff.lastIndexOf(".");

        QString exten = suff.mid(idxx);
        suff = suff.mid(0, idxx);

        folderPath.append("/" + suff + "_boxes" + exten);
        single.save(folderPath);
        ui->progressLabel->setText("Processed image saved");
    }

    if(ui->imgFolRadio->isChecked() && (!(procImages.isEmpty()))){
        QString file = ourImages[pixPlace];
        int idx = file.lastIndexOf("/");

        QString suff = file.mid(idx+1);
        int idxx = suff.lastIndexOf(".");

        QString exten = suff.mid(idxx);
        suff = suff.mid(0, idxx);

        folderPath.append("/" + suff + "_boxes" + exten);
        procImages[pixPlace].save(folderPath);
        ui->progressLabel->setText("Processed image saved");
    }
  }
}

void MainWindow::on_saveAllBtn_clicked(){

    if((!(ui->saveEdit->text().isEmpty())) ){

        QString folderPath = ui->saveEdit->text();

    if(ui->imageRadio->isChecked() && (!(single.isNull()))){

        QString file = ui->videoEdit->text();
        int idx = file.lastIndexOf("/");

        QString suff = file.mid(idx+1);
        int idxx = suff.lastIndexOf(".");

        QString exten = suff.mid(idxx);
        suff = suff.mid(0, idxx);

        folderPath.append("/" + suff + "_boxes" + exten);
        single.save(folderPath);
        ui->progressLabel->setText("Processed image saved");
    }

    if(ui->imgFolRadio->isChecked() && (!(procImages.isEmpty()))){

        QString keepFolPath = folderPath.mid(0);
      for(int i = 0; i < ourImages.length(); i++){
        QString file = ourImages[i];
        int idx = file.lastIndexOf("/");

        QString suff = file.mid(idx+1);
        int idxx = suff.lastIndexOf(".");

        QString exten = suff.mid(idxx);
        suff = suff.mid(0, idxx);

        folderPath.append("/" + suff + "_boxes" + exten);
        procImages[i].save(folderPath);
        folderPath = keepFolPath.mid(0);

    }
      ui->progressLabel->setText("Processed images saved");
  }
}

}

void MainWindow::on_zoomDial_valueChanged(){


    if(ui->zoomDial->value() > zoomdial){

        ui->imageView->scale(1.1, 1.1);
        ui->imageView->show();

    }

    else{

        ui->imageView->scale(0.9, 0.9);
        ui->imageView->show();

    }

    zoomdial = ui->zoomDial->value();
}

void MainWindow::on_zoomSlider_valueChanged(){


    if(ui->zoomSlider->value() > zoomslider){

        ui->imageView->scale(1.1, 1.1);
        ui->imageView->show();

    }

    else{

        ui->imageView->scale(0.9, 0.9);
        ui->imageView->show();

    }

    zoomslider = ui->zoomSlider->value();
}


void MainWindow::on_valueChoice_currentIndexChanged(){
    if(ui->imgFolRadio->isChecked()){

        if(ui->valueChoice->currentIndex() == 0){
            ui->numDefectEdit->setText(QString::number(numDefectList[pixPlace]));
        }

        if(ui->valueChoice->currentIndex() == 1){
            int sum = 0;
            for(int i = 0; i < numDefectList.length(); i++){
                sum += numDefectList[i];
            }
            ui->numDefectEdit->setText(QString::number(sum));
        }

        if(ui->valueChoice->currentIndex() == 2){
            double sum = 0.0;
            for(int i = 0; i < numDefectList.length(); i++){
                sum += numDefectList[i];
            }
            ui->numDefectEdit->setText(QString::number(sum/numDefectList.length()));

    }

    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
