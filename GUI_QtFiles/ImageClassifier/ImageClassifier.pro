#-------------------------------------------------
#
# Project created by QtCreator 2019-06-27T09:32:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageClassifier
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h

FORMS += \
        mainwindow.ui

win32: {
    contains(QMAKE_HOST.name, V7TMDFCPU4):{
        INCLUDEPATH += C:/Users/4gl/Documents/opencv/build/include

        LIBS += -LC:/Users/4gl/Documents/opencv/build/x64/vc15/lib \
                -LC:/Users/4gl/Documents/opencv/build/x64/vc15/bin

        CONFIG(release,debug|release){
            LIBS += -lopencv_world410 \
                -lopengl32 \
                -lglu32
        }

        CONFIG(debug,debug|release){
            LIBS += -lopencv_world410d \
                -lopengl32 \
                -lglu32
         }
    }
}



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
