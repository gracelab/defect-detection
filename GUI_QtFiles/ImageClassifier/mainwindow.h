#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QMessageBox>
#include <QDebug>
#include <QFile>
#include <QGraphicsPixmapItem>
#include <QCloseEvent>
#include <QElapsedTimer>
#include <QFileDialog>
#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_browseImgFolBtn_clicked();
    void on_browseImageBtn_clicked();
    void on_startBtn_clicked();
    void on_prevButton_clicked();
    void on_nextButton_clicked();
    void on_pbBrowseBtn_clicked();
    void on_pbtxtBrowseBtn_clicked();
    void on_classesBrowseBtn_clicked();
    void on_browseSaveBtn_clicked();
    void on_saveButton_clicked();
    void on_saveAllBtn_clicked();
    void on_zoomDial_valueChanged();
    void on_zoomSlider_valueChanged();
    void on_valueChoice_currentIndexChanged();


private:
    Ui::MainWindow *ui;

private:
    QPixmap image;
    QImage *imageObject;
    QGraphicsScene scene;
    QStringList ourImages;
    int place;
    cv::dnn::Net tfNetwork;
    QGraphicsPixmapItem pixmap;
    QList<QPixmap> pixList;
    int pixPlace;
    QImage single;
    QList<QImage> procImages;
    int zoomdial;
    int zoomslider;
    QList<int> numDefectList;
    QString singleImgHold;

};



#endif // MAINWINDOW_H
