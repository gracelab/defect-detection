import os
import datetime
import random
from shutil import copyfile, copy2
import math
import xml_to_csv


def generate_random():

    currDate = datetime.datetime.now()
    print(str(currDate))

    trainFolName = "train_D" + str(currDate)[5:7] + "-" + str(currDate)[8:10] + "-" + str(currDate)[2:4] + \
                   "_T" + str(currDate)[11:13] + "-" + str(currDate)[14:16] + "-" + str(currDate)[17:19]
    testFolName = "test_D" + str(currDate)[5:7] + "-" + str(currDate)[8:10] + "-" + str(currDate)[2:4] + \
                   "_T" + str(currDate)[11:13] + "-" + str(currDate)[14:16] + "-" + str(currDate)[17:19]

    allImgPath = r"C:\Users\4gl\Documents\TensWorkspace\training_demo\images\allpng_alt_noren"
    allXMLPath = r"C:\Users\4gl\Documents\TensWorkspace\training_demo\images\allxml_alt_noren"
    #cirImg = r"C:\Users\4gl\Pictures\Shapes\Circle"
    #cirtx = r"C:\Users\4gl\Pictures\Shapes\CirTxt"

    tups = randImgXMLList(allImgPath, allXMLPath)
    #tups = randImgXMLList(cirImg, cirtx)

    trainpath = r"C:\Users\4gl\Documents\TensWorkspace\training_demo\images\randomtrain"
    testpath = r"C:\Users\4gl\Documents\TensWorkspace\training_demo\images\randomtest"

    bigTup = makeImg_XMLFol(trainpath, testpath, trainFolName, testFolName, tups)


    trainFol = bigTup[0]
    testFol = bigTup[1]
    trainXML = bigTup[2]
    testXML = bigTup[3]

    trainanno = r"C:\Users\4gl\Documents\TensWorkspace\training_demo\annotations\randomtrainlabels"
    testanno = r"C:\Users\4gl\Documents\TensWorkspace\training_demo\annotations\randomtestlabels"


    smallerTup = conv_xml_to_csv(trainanno, testanno, trainFolName, testFolName, trainXML, testXML)

    traincsv = smallerTup[0]
    testcsv = smallerTup[1]

    smallerTup2 = conv_csv_to_tfrecord(trainanno, testanno, trainFolName, testFolName, traincsv, testcsv, trainFol, testFol)

    trainrecord = smallerTup2[0]
    testrecord = smallerTup2[1]

    print("------------------------------------------------------------------------------------------------------------------")
    print("Random training image set located at: " + trainFol)
    print("Random testing image set located at: " + testFol)
    print("------------------------------------------------------------------------------------------------------------------")
    print("Random training image XML files located at: " + trainXML)
    print("Random testing image XML files located at: " + testXML)
    print("------------------------------------------------------------------------------------------------------------------")
    print("Random training image CSV file (from XML files) located at: " + traincsv)
    print("Random testing image CSV file (from XML files) located at: " + testcsv)
    print("------------------------------------------------------------------------------------------------------------------")
    print("Random training image TFRecord file located at: " + trainrecord)
    print("Random testing image TFRecord file located at: " + testrecord)
    print("------------------------------------------------------------------------------------------------------------------")
    print("All conversions complete")


def conv_csv_to_tfrecord(trainanno, testanno, trainFolName, testFolName, traincsv, testcsv, trainFol, testFol):

    trainrecord = os.path.join(trainanno, trainFolName)
    testrecord = os.path.join(testanno, testFolName)
    trainrecord = trainrecord + ".record"
    testrecord = testrecord + ".record"

    os.system("python generate_tfrecord.py --label=defect --csv_input=" + traincsv + " --img_path=" + trainFol + " --output_path=" + trainrecord)
    os.system("python generate_tfrecord.py --label=defect --csv_input=" + testcsv + " --img_path=" + testFol + " --output_path=" + testrecord)

    return(trainrecord, testrecord)


def conv_xml_to_csv(trainanno, testanno, trainFolName, testFolName, trainXML, testXML):

    newtrainlab = os.path.join(trainanno, trainFolName)
    newtestlab = os.path.join(testanno, testFolName)
    newtrainlab = newtrainlab + "_labels.csv"
    newtestlab = newtestlab + "_labels.csv"

    os.system("python xml_to_csv.py -i " + trainXML + " -o " + newtrainlab)
    os.system("python xml_to_csv.py -i " + testXML + " -o " + newtestlab)

    return (newtrainlab, newtestlab)


def randImgXMLList(allImgPath, allXMLPath):

    newlist = []
    for file in os.listdir(allImgPath):
        filename = os.fsdecode(file)
        filename = os.path.join(allImgPath, filename)
        idx = filename.find("Layer")
        idxd = filename.find(".")
        if idx != -1 & idxd != -1:
            filealt = filename[idx:idxd] + ".xml"
            xfilename = os.path.join(allXMLPath, filealt)

            newlist.append((filename, xfilename))
    random.shuffle(newlist)
    print(newlist)
    return newlist



def makeImg_XMLFol(trainpath, testpath, trainFolName, testFolName, tups):
    oldtrainpath = trainpath
    oldtestpath = testpath
    trainpath = os.path.join(trainpath, trainFolName)
    testpath = os.path.join(testpath, testFolName)

    trainFolName = trainFolName + "xml"
    testFolName = testFolName + "xml"

    traintxtpath = os.path.join(oldtrainpath, trainFolName)
    testtxtpath = os.path.join(oldtestpath, testFolName)

    os.mkdir(trainpath)
    os.mkdir(testpath)
    os.mkdir(traintxtpath)
    os.mkdir(testtxtpath)

    cutPoint = math.ceil(len(tups)*0.8)

    traintups = tups[:cutPoint]
    testtups = tups[cutPoint:]
    for item in traintups:
        copy2(item[0], trainpath)
        copy2(item[1], traintxtpath)
    for item in testtups:
        copy2(item[0], testpath)
        copy2(item[1], testtxtpath)

    return (trainpath, testpath, traintxtpath, testtxtpath)



if __name__ == "__main__":

    generate_random()