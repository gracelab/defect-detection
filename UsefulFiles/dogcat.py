import tensorflow as tf
from tensorflow import keras
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import os
import cv2
import random


#Change directory to your image path
DATADIR = r"C:\Users\4gl\Pictures\PetImages"

CATEGORIES = ["Dog", "Cat"]

training_data = []

def create_training_data():
    for category in CATEGORIES:
        path = os.path.join(DATADIR, category)
        class_num = CATEGORIES.index(category)
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array, (225, 225))
                training_data.append([new_array, class_num])
            except Exception as e:
                pass

create_training_data()


random.shuffle(training_data)

trainx = []
labely = []
for arr, lab in training_data:
    trainx.append(arr)
    labely.append(lab)


trainingx = trainx[:19957]
traininglabel = labely[:19957]

testx = trainx[19957:]
testlabel = labely[19957:]

trainingx = np.array(trainingx).reshape(-1, 225, 225, 1)
testx = np.array(testx).reshape(-1, 225, 225, 1)

print(trainingx.shape)
print(testx.shape)
trainingx = trainingx / 255.0
testx = testx/255.0


#Create model architecture
myModel = keras.Sequential()
myModel.add(keras.layers.Conv2D(32, kernel_size=(3, 3), activation = "relu", input_shape = trainingx.shape[1:]))
myModel.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
myModel.add(keras.layers.Dropout(0.2))

myModel.add(keras.layers.Conv2D(32, kernel_size=(3, 3), activation = "relu"))
myModel.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
myModel.add(keras.layers.Dropout(0.2))

myModel.add(keras.layers.Flatten())
myModel.add(keras.layers.Dense(128, activation = "relu"))
#myModel.add(keras.layers.Dropout(0.2))

myModel.add(keras.layers.Dense(1, activation = "sigmoid"))


myModel.summary()


#Train the model
print("---------------------First Session---------------------")

myModel.compile(optimizer='adam',
                loss='binary_crossentropy',
                metrics=['accuracy'])

myModel.fit(trainingx, traininglabel, epochs = 5, batch_size=32)

testLoss, testAcc = myModel.evaluate(testx, testlabel)
print("Testing accuracy: " + str(testAcc))

predictions = myModel.predict(testx)
print(np.argmax(predictions[0]))
print(testlabel[0])

myModel.summary()

myModel.save(r"C:\Users\4gl\PyProjects\venv\dogcat_savedmodels\model_7ep_drop4.h5")


print("---------------------First Session Done---------------------")

#Can just run this section (and not model architecture or model training sections) if you have trained a model;
#change path in line 104 to your model path
print("---------------------H5 Save and Reload---------------------")
new_model = keras.models.load_model(r"C:\Users\4gl\PyProjects\venv\dogcat_savedmodels\model_7ep_drop4.h5")
new_model.summary()
testLoss, testAcc = new_model.evaluate(testx, testlabel)
print("Testing accuracy: " + str(testAcc))
print("---------------------H5 Save and Reload Done---------------------")
print("---------------------Complete---------------------")