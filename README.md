# Using TensorFlow and Qt Creator to Train a Neural Network and Create a Graphical Application that Detects Defects on CT Scans of 3D Printed Parts
This repository contains files and instructions for using TensorFlow's Object Detection API to train a neural network to identify and draw bounding boxes around defects on CT scans of 
3D printed parts. Then, the trained model can be exported and used in the included graphical application, developed in Qt Creator. These steps were completed using Windows 10, Python 3.7, and
Anaconda 2019.07.
## Training a Neural Network to Find Defects
Note: This section is to explain how the models in the repository were created and to provide a guide for those who want to train their own model on their own images.
You can simply clone the repository (since it contains a trained model) and skip to the "Running the Graphical User Interface" section if you only want to try out the application, not train your own network.
### Create Workspace
1. In Documents, create a folder called `Workspace` with the following structure:
```
Workspace
    - training_demo
        -annotations
            -test
            -train
        -evaluation
        -images
            -test
            -train
            -testxml
            -trainxml
        -pre-trained-model
        -training
```
2. Download the ssd_inception_v2_coco model from this [link](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md). This is
a pre-trained object detection model that will be tailored for detecting defects. Unzip the ssd_inception_v2_coco_2018_01_28.tar.gz file using software like WinZip or 7-Zip,
and extract the model to \Documents\Workspace\training_demo\pre-trained-model.

### Installing Required Software and Files
#### 1. Anaconda
Download Anaconda 3.7 from this [link](https://www.anaconda.com/distribution/#download-section), and follow the installer. Anaconda Navigator and the Anaconda Command Prompt can 
be used to access Anaconda, so it does not need to be added to your PATH variable. Select that Anaconda should be registered as your default Python. If you have not installed
PyCharm, you can follow the link in the installer to get PyCharm for Anaconda.
#### 2. PyCharm
Download the PyCharm Community version from this [link](https://www.jetbrains.com/pycharm/download/#section=windows), and run the .exe file to complete installation. You can also
install PyCharm for Anaconda during the Anaconda installation.
##### Set Up PyCharm and Download Other Software from Anaconda
In PyCharm, go to File -> Settings -> Project Interpreter, and by clicking the gear icon, add a new interpreter that is the location of the python.exe file in your conda environment.  
In the PyCharm Terminal, activate your conda environment by typing `activate [environment name]`, replacing [environment name] with the name of your environment. Upgrade pip:  
```python
python -m pip install --upgrade pip
```
Install the following packages as shown:
```python
conda install -c anaconda protobuf
pip install tensorflow
pip install pillow
pip install lxml
pip install Cython
pip install contextlib2
pip install jupyter
pip install matplotlib
pip install pandas
pip install opencv-python
```
#### 3. TensorFlow Object Detection API
Download TensorFlow's Object Detection repository from this [link](https://github.com/tensorflow/models) by clicking "Clone or Download" and "Download ZIP". Open the ZIP file and
extract the models-master folder into your Documents folder.
##### 3a. Configure PYTHONPATH
Create an environment variable named PYTHONPATH that points to \models-master, \models-master\research, and \models-master\research\slim by typing in your Terminal:
```python
set PYTHONPATH=[path-to-Documents]\Documents\models-master;[path-to-Documents]\Documents\models-master\research;[path-to-Documents]\Documents\models-master\research\slim
```
##### 3b. Protobufs and setup.py
Protobufs are used by TensorFlow to configure the parameters of the models. In the PyCharm Terminal, with your conda environment activated, change to the \models-master\research directory:
```python
cd [path-to-Documents]\Documents\models-master\research
```
Then copy and run this command:
```python
protoc --python_out=. .\object_detection\protos\anchor_generator.proto .\object_detection\protos\argmax_matcher.proto .\object_detection\protos\bipartite_matcher.proto .\object_detection\protos\box_coder.proto .\object_detection\protos\box_predictor.proto .\object_detection\protos\eval.proto .\object_detection\protos\faster_rcnn.proto .\object_detection\protos\faster_rcnn_box_coder.proto .\object_detection\protos\grid_anchor_generator.proto .\object_detection\protos\hyperparams.proto .\object_detection\protos\image_resizer.proto .\object_detection\protos\input_reader.proto .\object_detection\protos\losses.proto .\object_detection\protos\matcher.proto .\object_detection\protos\mean_stddev_box_coder.proto .\object_detection\protos\model.proto .\object_detection\protos\optimizer.proto .\object_detection\protos\pipeline.proto .\object_detection\protos\post_processing.proto .\object_detection\protos\preprocessor.proto .\object_detection\protos\region_similarity_calculator.proto .\object_detection\protos\square_box_coder.proto .\object_detection\protos\ssd.proto .\object_detection\protos\ssd_anchor_generator.proto .\object_detection\protos\string_int_label_map.proto .\object_detection\protos\train.proto .\object_detection\protos\keypoint_box_coder.proto .\object_detection\protos\multiscale_anchor_generator.proto .\object_detection\protos\graph_rewriter.proto .\object_detection\protos\calibration.proto .\object_detection\protos\flexible_grid_anchor_generator.proto
```
Finally, from the same directory, run these commands:
```python
python setup.py build
python setup.py install
```
##### 3c. COCO API Download
To use COCO evaluation metrics, download the COCO API from this [link](https://github.com/cocodataset/cocoapi) and copy the pycocotools folder to the
\models-master\research directory. Add `metrics_set: "coco_detection_metrics"` to the `eval_config` field in your model's configuration file (described later).

##### Test Installation
Test your installation of the TensorFlow Object Detection API by running the following command from the \models-master\research directory:
```python
python object_detection\builders\model_builder_test.py
```
#### 4. OpenCV
Download and install OpenCV using the instructions for Windows at this [link](https://docs.opencv.org/master/d3/d52/tutorial_windows_install.html).

### Processing Images
Note: You can also use the pre-labeled images, XML files, and CSV files found in this repository.
#### Gathering Images
To train the network, you'll need many training images -- at least 300 should be fairly sufficient. The model in the repository is trained on about 883 images. Taking more diverse
images (i.e. defects in varying locations on the images, defects of many different shapes/sizes, etc.) will yield a more general model that is less confined to a specific type
of image. Try not to use images larger than about 200 KB or 900 x 900, as this will start greatly increasing the time it takes to train the model.  
Use 80% of the images for training and 20% for testing. Place the training images in \Documents\Workspace\training_demo\images\train and place the testing images in
\Documents\Workspace\training_demo\images\test.

#### Labeling Images
##### 1. Download LabelImg Software
Download LabelImg at this [link](https://github.com/tzutalin/labelImg). Extract the labelImg-master folder to Documents. Open Anaconda Prompt, activate your conda environment, and
cd to the labelImg-master folder: `cd [path-to-Documents]\Documents\labelImg-master`. Then, run `python labelImg.py` to launch LabelImg.
##### 2. Drawing Bounding Boxes and Receiving XML Files
Select `Open Dir` and choose the folder that contains your training (or testing) images. Select `Change Save Dir` and choose [path-to-Documents]\Documents\Workspace\training_demo\images\trainxml (or \testxml).
Using the Create RectBox feature, draw a bounding box around each defect in an image and click Save. Each bounding box should have the label "defect". After doing this for each
image, your \images\trainxml (or \images\testxml) folder should have an XML file for every image, which contains the coordinates of every bounding box in the given image.
#### Creating Annotations
1. Download `xml_to_csv.py` from this repository, and use it to convert your XML files to CSV. After downloading the file, a sample XML to CSV conversion would look like:
```python
python xml_to_csv.py -i [PATH_TO_TRAINXML_FOLDER]\trainxml -o [PATH_TO_ANNOTATIONS_FOLDER]\train\train_labels.csv
```
or
```python
python xml_to_csv.py -i [PATH_TO_TESTXML_FOLDER]\testxml -o [PATH_TO_ANNOTATIONS_FOLDER]\test\test_labels.csv
```
2. Download `generate_tfrecord.py` from this repository, and use it to convert your CSV files to TFRecord. After downloading the file, a sample CSV to TFRecord conversion would look like:
```python
python generate_tfrecord.py --label=defect --csv_input=<PATH_TO_ANNOTATIONS_FOLDER>\train\train_labels.csv  --output_path=<PATH_TO_ANNOTATIONS_FOLDER>\train\train.record
```
or
```python
python generate_tfrecord.py --label=defect --csv_input=<PATH_TO_ANNOTATIONS_FOLDER>\test\test_labels.csv  --output_path=<PATH_TO_ANNOTATIONS_FOLDER>\test\test.record
```
Note: In this repository, the CSV files for training and testing are found at `Workspace\training_demo\annotations\train` and `Workspace\training_demo\annotations\test`, but the
TFRecord files were too large to include in the repository. You can create them by using `generate_tfrecord.py` from `Workspace\training_demo\annotations` on the included CSV files.

### Label Map and Configuration
#### Label Map
Download `defect_label_map.pbtxt` from this repository, and place it in [path-to-Documents]\Documents\Workspace\training_demo\training.
Also download `defect_label_map.txt` from this repository -- this will be used in running the graphical application, after the model is trained.
#### Configuring the Model
1. Navigate to [path-to-Documents]\Documents\models-master\research\object_detection\samples\configs, and copy `ssd_inception_v2_coco.config` into the
[path-to-Documents]\Documents\Workspace\training_demo\training folder.
2. Make the following changes to the file, using single forward slashes (not backslashes) for file paths:  
  * Change the num_classes field to the number of object classes you want to detect -- in this case, "defect" is the only class, so change the line to `num_classes: 1`.  
  * Change the fine_tune_checkpoint field to: `fine_tune_checkpoint: "[path-to-Documents]/Documents/TensWorkspace/training_demo/pre-trained-model/ssd_inception_v2_coco_2018_01_28/model.ckpt"`.  
  * Navigate to the train_input_reader section, and change input_path and label_map_path to:  
    * `input_path: "[path-to-Documents]/Documents/Workspace/training_demo/annotations/train/train.record"`  
    * `label_map_path: "[path-to-Documents]/Documents/Workspace/training_demo/training/defect_label_map.pbtxt"`  
  * Navigate to the eval_config section, and change the num_examples field to: `num_examples: [number of images in your testing set]` (i.e. `num_examples: 220`)  
  * Navigate to the eval_input_reader section, and change input_path and label_map_path to:  
    * `input_path: "[path-to-Documents]/Documents/Workspace/training_demo/annotations/test/test.record"`  
    * `label_map_path: "[path-to-Documents]/Documents/Workspace/training_demo/training/defect_label_map.pbtxt"`  

### Training the Model
From the [path-to-Documents]\Documents\models-master\research\object-detection directory, run the following command:
```python
python train.py --logtostderr --train_dir=[path-to-Documents]\Documents\Workspace\training_demo\training --pipeline_config_path=[path-to-Documents]\Documents\Workspace\training_demo\training\ssd_inception_v2_coco.config
```
After a bit of time for setup, the model should begin training, displaying the number of steps and current loss in the terminal as it progresses.
#### Using TensorBoard
You can launch TensorBoard to see graphs of the model's loss, steps, learning rate, etc. as they train. Open Anaconda Prompt, activate your conda environment with `activate [name of environment]`,
and change directories to the [path-to-Documents]\Documents\models-master\research\object-detection folder. Run the command:
```python
tensorboard --logdir=[path-to-Documents]\Documents\Workspace\training_demo\training
```
This creates a webpage on your computer at [YourComputerName]:6006, which can be viewed in a web browser to observe training progress.
### Exporting the Trained Model
Note: Training can be terminated by pressing Ctrl+C in the Terminal, but because the exports simply use the last saved checkpoint, training can still continue while the model is being exported.
#### Inference Graph (.pb File)
From the [path-to-Documents]\models-master\research\object-detection directory, run this command, replacing "XXXX" with the number of the most recent saved checkpoint (i.e., model.ckpt-8564):
```python
python export_inference_graph.py --input_type image_tensor --pipeline_config_path [path-to-Documents]\Documents\Workspace\training_demo\training\ssd_inception_v2_coco.config --trained_checkpoint_prefix [path-to-Documents]\Documents\Workspace\training_demo\training\model.ckpt-XXXX --output_directory [path-to-Documents]\Documents\Workspace\training_demo\training\inference_graph
```
This creates `frozen_inference_graph.pb` in the \training\inference_graph folder.
#### Optional: Evaluation
Using `frozen_inference_graph.pb`, you can get evaluation statistics on the model if you wish.  
In the PyCharm Terminal, make sure you have activated your conda environment. Change directories to [path-to-Documents]\Documents\models-master\research\object_detection\legacy,
and run the following command:
```python
python eval.py --logtostderr --pipeline_config_path=[path-to-Documents]\Documents\Workspace\training_demo\training\ssd_inceptionv2_coco.config --checkpoint_dir=[path-to-Documents]\Documents\Workspace\training_demo\training --eval_dir=[path-to-Documents]\Documents\Workspace\training_demo\evaluation
```
This will provide you with statistics on the classification loss, localization loss, mean average precision, etc., of your trained model.
#### Convert Inference Graph to .pbtxt
Navigate to your OpenCV download, and go to [path-to-opencv]\opencv\sources\samples\dnn. Copy `tf_text_graph_ssd.py` and `tf_text_graph_common.py` and paste them into
your [path-to-Documents]\Documents\Workspace\training_demo\training folder. Run the following command from the \training folder:
```python
python tf_text_graph_ssd.py --input inference_graph\frozen_inference_graph.pb --output frozen_inference_graph.pbtxt --config [path-to-Documents]\Documents\Workspace\training_demo\training\ssd_inception_v2_coco.config
```
This should create `frozen_inference_graph.pbtxt` in your \training folder.  
With `frozen_inference_graph.pb` and `frozen_inference_graph.pbtxt` created, you are ready to observe the trained model in the graphical application.

#### Note: .pbtxt Conversion Trouble
In addition to the original `tf_text_graph_ssd.py` file, this repository also contains a file called `tf_text_graph_ssd_newvers.py`, which has some added code to deal with errors related to
sorted nodes in the .pb inference graph. If you use `tf_text_graph_ssd.py` and receive an error that states:
```python
Traceback (most recent call last):
File "tf_text_graph_ssd.py", line 138, in 
assert(graph_def.node[0].op == 'Placeholder')
AssertionError
```
you can try `tf_text_graph_ssd_newvers.py` by first un-commenting lines 140-147. Then run the command:
```python
python tf_text_graph_ssd_newvers.py --input inference_graph\frozen_inference_graph.pb --output frozen_inference_graph.pbtxt --config [path-to-Documents]\Documents\Workspace\training_demo\training\ssd_inception_v2_coco.config
```
This operation may hit an error and terminate, but a file in your \training folder should appear called `sorted_inference_graph.pb`. Comment out lines 140-147, and run the command:
```python
python tf_text_graph_ssd_newvers.py --input sorted_inference_graph.pb --output frozen_inference_graph.pbtxt --config [path-to-Documents]\Documents\Workspace\training_demo\training\ssd_inception_v2_coco.config
```
With the nodes properly sorted in `sorted_inference_graph.pb`, the file should now run properly and finish building `frozen_inference_graph.pbtxt`.

## Running the Graphical User Interface
### Installing Required Software and Files
#### OpenCV
Download and install OpenCV using the instructions for Windows at this [link](https://docs.opencv.org/master/d3/d52/tutorial_windows_install.html).
#### CMake
Download and install CMake at this [link](https://cmake.org/download/).
#### Qt Creator
Qt Creator can be downloaded at this [link](https://www.qt.io/download), and instructions for integrating OpenCV, CMake, and Qt Creator can be found at this [link](https://wiki.qt.io/How_to_setup_Qt_and_openCV_on_Windows).
#### Microsoft Visual Studio
Microsoft Visual Studio 2017 was used as the kit for this project, which can be downloaded and installed at this [link](https://visualstudio.microsoft.com/vs/older-downloads/).
Once it is installed, it can be changed to the default kit by opening Qt Creator, going to Tools -> Options -> Kit and choosing `Desktop Qt 5.12.3 MSVC2017 64bit` as the default kit.
### Clone/Download Repository
Download the files in this repository, and extract them to your Documents folder.
### Running Project in Qt Creator
In Qt Creator, select File -> Open File or Project, and select the [path-to-Documents]\material-defects-master\GUI_QtFiles\ImageClassifier folder from the downloaded repository files. You should be able to click the green arrow
in the bottom left of Qt Creator and run the application. Then, enter your saved model by putting your .pb and .pbtxt files in the appropriate fields in the Detector Model tab, along with a .txt 
version of the label map. In the Images tab, select Browse and choose either an image or a folder of images to be evaluated. Press Start, and your images should be labeled
with bounding boxes. You can use other features that allow you to zoom, view the total or average number of defects, save the images with bounding boxes, and more.
## Extra: Constructing a 3D Model of the Material
If you would like to create a 3D model of your material by using images with bounding boxes from your neural network, you can easily do so using the graphical application and ImageJ software.
1. Run all images through the network in the graphical application and save them to your computer. You should then have a folder that contains all of your images with bounding boxes denoting defects.
2. Using any image editing software, like Photoshop, GIMP, or even Paint 3D, create a mask of each image by coloring defect regions white and all other space black.
3. Download FIJI ImageJ software at this [link](https://fiji.sc/).
4. Open FIJI ImageJ (by selecting `ImageJ-win64.exe` in your downloaded ImageJ folder) and select File -> Import -> Image Sequence. Navigate to your folder of image masks, select
one image, and FIJI should open the entire image set.
5. Select Plugins -> 3D Viewer, and FIJI will construct a 3D model by stacking your image masks.

#### Note: Where to find files on original PC
---
The exact files in this repository can be found at `C:\Users\4gl\Documents\GUI_QtFiles` and `C:\Users\4gl\Documents\Workspace`.  
The other files I worked on are also all found in Documents:
1. Some miscellaneous files I used near the beginning of my appointment are in `C:\Users\4gl\Documents\PyProjects\venv`, such as:
  * `dogcat.py` - creates and trains a model to classify images as dogs or cats using TensorFlow/Keras
  * `generate_tfrecord.py` - converts CSV files to TFRecord
  * `random_train_test.py` - takes a folder of images and XML files, randomly splits them 80%/20% into training and testing groups, and creates appropriate CSV and TFRecord files
  * `readtfrecord.py` - prints output of a TFRecord file (used for checking that TFRecord contents are correct)
  * `tf_cv_test.py` - takes .pb and .pbtxt version of a trained neural network, along with a test image, and uses OpenCV to display the bounding boxes detected on the image
  * `xml_to_csv.py` - converts XML files to CSV

2. The TensorFlow Object Detection API is found at `C:\Users\4gl\Documents\models-master`.
3. My files and pictures for the ORISE poster session are found at `C:\Users\4gl\Documents\Poster`.
4. My files and pictures for the MDF Peer Review poster session are found at `C:\Users\4gl\Documents\Poster_Lite`.
5. My Qt Creator applications are found at `C:\Users\4gl\Documents\QtThings`, such as:
  * `ImageClassifier` folder - the final version of the defect detection API
  * `QtTest` folder - small GUI application just for testing Qt features and capabilities
6. The files for training my object detection neural networks are found at `C:\Users\4gl\Documents\TensWorkspace\training_demo`, such as:
  * `annotations` folder:
    * `traintf.record` and `testtf.record` - original training and testing sets in TFRecord format for first training of neural network
    * `randomtestlabels` and `randomtrainlabels` folders - contain CSV and TFRecord files for corresponding training and testing sets (randomly generated by `random_train_test.py`), and corresponding training/testing pairs can be identified by matching numbers in file names
  * `evaluation` folder - contains some TensorBoard webpages that were used to observe training and a copy of the model's configuration file (`pipeline.config`)
  * `images` folder - contains all images used throughout my appointment, separated into various folders. Most important folders include:
    * `allpng` folder - contains 703 of the original images converted to PNG and made into size 500 x 500. (Excludes 13 images at the end of the original set that had "27" melted into layers)
    * `allpng_alt` folder - contains all images in `allpng`, includes 432 new altered images (200 from original set that are horizontally flipped, 200 from original set that are rotated counterclockwise 90 degrees, and 32 from new set with defects in the centers of the images)
    * `allxml` and `allxml_alt` folders - contain corresponding XML files for images in `allpng` and `allpng_alt`
    * `allpng_boxes` folder - all 716 original images that were run through the trained neural network to obtain bounding boxes around defects
    * `allpng_masks` folder - masks of all 716 images (white used for defect regions, black used for all other space). Masks were put into FIJI ImageJ to create 3D model.
    * Any folder beginning with `newimages` or `new_images` - new image set from a 3D printed "dog bone" part, used for testing trained model's accuracy on unfamiliar images
    * `trainpng` and `testpng` folders - original training and testing sets (from original 716 images) for first training of neural network. These images are PNG; the TIF versions are in the `train` and `test` folders.
    * `randomtest` and `randomtrain` folders - contain folders of corresponding testing and training sets of images and XML files. (Training/testing set pairs can be identified using matching numbers in folder names)
    * `twentysevens` and `twentysevensxml` folders - contain the 13 images with "27" burned into the layers and the corresponding XML files
  * `pre-trained-model` folder - contains the checkpoints and configuration file for the `ssd_inception_v2_coco` model, downloaded from GitHub
  * `training` folder - contains checkpoints and saved graphs of the trained neural networks. Includes:
    * In `\training` (the first training of neural network, no parameters changed) :
      * Saved checkpoints
      * `defect_label_map.pbtxt` and `defect_label_map.txt` - former is the label map used for training, latter is the label map used in GUI
      * `frozen_inference_graph.pbtxt` - saved graph in .pbtxt format used in GUI
      * `inference_graph\frozen_inference_graph.pb` - saved graph in .pb format used in GUI
      * `pipeline.config` - configuration file for this training
      * `tf_text_graph_ssd_newvers.py` and `tf_text_graph_common.py` for creating .pbtxt file
    * In `\training\train2_6_28_19` (second training with batch size changed from 32 to 24):
      * Saved checkpoints
      * `frozen_inference_graph.pbtxt` - saved graph in .pbtxt format used in GUI
      * `inference_graph\frozen_inference_graph.pb` - saved graph in .pb format used in GUI
      * `pipeline.config` - configuration file for this training
      * `tf_text_graph_ssd_newvers.py` and `tf_text_graph_common.py` for creating .pbtxt file
    * In `training\train3_7_3_19_ALT` (third training with expanded training set):
      * Saved checkpoints
      * `pipeline.config` - configuration file for this training
      * `tf_text_graph_ssd_newvers.py` and `tf_text_graph_common.py` for creating .pbtxt file
      * `\firstgraphs\inference_graph_1\frozen_inference_graph.pb` - saved graph in .pb format used in GUI (saved at ~14,000 steps)
      * `\firstgraphs\frozen_inference_graph.pbtxt` - saved graph in .pbtxt format used in GUI (saved at ~14,000 steps)
      * `\secondgraphs\inference_graph\frozen_inference_graph.pb` - saved graph in .pb format used in GUI (saved at ~40,000 steps)
      * `\secondgraphs\frozen_inference_graph.pbtxt` - saved graph in .pbtxt format used in GUI (saved at ~40,000 steps)
      * `\thirdgraphs_104292\inference_graph\frozen_inference_graph.pb` - saved graph in .pb format used in GUI (saved at ~100,000 steps)
      * `\thirdgraphs_104292\frozen_inference_graph.pbtxt` - saved graph in .pbtxt format used in GUI (saved at ~100,000 steps)
      * `\fourthgraphs\inference_graph\frozen_inference_graph.pb` - saved graph in .pb format used in GUI (saved at ~115,000 steps) (This is the model included in this repository)
      * `\fourthgraphs\frozen_inference_graph.pbtxt` - saved graph in .pbtxt format used in GUI (saved at ~115,000 steps) (This is the model included in this repository)

7. Miscellaneous software found at `C:\Users\4gl\Documents`:
  * CMake - `\cmake-3.15.0-rc1-win64-x64`
  * COCO API - `\cocoapi-master`
  * FIJI ImageJ - `\fiji-win64`
  * LabelImg - `\labelImg-master`
  * OpenCV - `\opencv`
  * Microsoft Visual Studio 2017 - `\Visual Studio 2017`